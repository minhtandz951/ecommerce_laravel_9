<div>
    <div class="container" style="padding: 30px 0">
     <div class="row">
         <div class="col-md-12">
             <div class="panel panel-default">
                 <div class="panel-heading">
                     <div class="row">
                         <div class="col-md-6">
                             All Products
                         </div>
                         <div class="col-md-6">
                             <a href="{{route('admin.addproduct')}}" class="btn btn-success pull-right">Add new </a>
                         </div>
                     </div>
                 </div>
                 <div class="panel-body">
                     @if(Session::has('message'))
                         <div class="alert alert-success" role="alert">{{Session::get('message')}}</div>
                     @endif
                     <table class="table table-striped">
                         <thead>
                             <tr>
                                 <th>Id</th>
                                 <th>Image</th>
                                 <th>Product name</th>
                                 <th>Stock</th>
                                 <th>Price</th>
                                 <th>Sale Price</th>
                                 <th>Category</th>
                                 <th>Date</th>
                                 <th>Action</th>

                             </tr>
                         </thead>
                         <tbody>
                             @foreach ($products as $key => $product)
                             <tr>
                                 <td>{{$product->id}}</td>
                                 <td><img src="{{asset('assets/images/products')}}/{{$product->image}}" width="60"/> </td>
                                 <td>{{$product->name}}</td>
                                 <td>{{$product->stock_status}}</td>
                                 <td>{{$product->regular_price}}</td>
                                 <td>{{$product->sale_price}}</td>
                                 <td>{{$product->category->name}}</td>
                                 <td>{{$product->created_at}}</td>
                                 <td>
                                      <a href="{{route('admin.editproduct', ['product_slug'=>$product->slug])}}">
                                         <i class="fa fa-edit fa-2x text-info"></i>
                                     </a> 
                                     <a href="#" onclick="confirm('Are you sure, You want to deleta this product')  || event.stopImmediatePropagation()" 
                                     wire:click.prevent = "deleteProduct({{$product->id}})" style="margin-left: 10px">
                                         <i class="fa fa-times fa-2x text-danger"></i>
                                     </a> 
                                 </td>
                             </tr>
                             @endforeach
                         </tbody>
                     </table>
                     {{$products->links('pagination::bootstrap-4')}}
                 </div>
             </div>
         </div>
     </div>
     </div>
 </div>
 
